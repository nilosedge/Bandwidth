import java.awt.*;
import java.net.*;
import java.io.*;
import java.awt.event.*;
import java.applet.Applet;
import QueueTools.*;


public class Bandwidth extends Applet implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6211202914421579840L;
	//private static final int DELAY = 10;
	protected Button login;
	protected TextField username;
	protected TextField password;
	protected Label usernameL;
	protected Label passwordL;
	protected Thread running = null;
	protected Label temp, free;
	protected boolean auth, loggedin = false;
	protected String maxin, maxout, curin, curout, ip, token;
   private Graphics gContext; // off-screen graphics context 
   private Image buffer; 
	protected static Socket fred;
	protected Queue queue;
	protected Node node;
	protected boolean debug = false;
	protected static int width = 200;
	protected static int height = 200;
	protected static int barwidth = 30;
	protected static int bar = height - barwidth;
	protected static int meg = (1024*1024);

	public void init() {

      buffer = createImage(width, height); // create image buffer
      gContext = buffer.getGraphics(); // get graphics context
      gContext.setColor(Color.black);
      gContext.fillRect(0, 0, width, height);
		
		queue = new Queue(width);
		setBackground(Color.white);
		setLayout(new FlowLayout());

		loggedin = login(getParameter("username"), getParameter("password"));

		if(loggedin != true) {
			usernameL = new Label("Username:");
			passwordL = new Label("Password:");
			add(usernameL);
			username = new TextField(24);
			add(username);
			add(passwordL); 
			password = new TextField(18);
			password.setEchoChar('*');
			add(password);
			login = new Button("Login");
			add(login);
			login.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						loggedin = login(username.getText(), password.getText());
						if(loggedin == true ) {
							remove(username);
							remove(password);
							remove(usernameL);
							remove(passwordL);
							remove(login);
						} else {
							System.out.println("You did not Authenticate");
						}
						repaint(); 
					} 
				}
			);
		}


		repaint();
		// running = new Thread(this);
	}

	public void start() { 
      gContext.setColor(Color.white);
      gContext.fillRect(0, 0, width, height);
	}

	public void run() {
		System.out.println("Welcome to the Fias internet applet");
		while(loggedin == true) {
			loggedin = sendtoken();
			repaint();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println("There was a problem with the thread");
			}
			if(loggedin == false) {
				while(loggedin == false) {
					loggedin = login(username.getText(), password.getText());
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						System.out.println("There was a problem with the thread");
					}
				}
			}
		}
		System.out.println("You are not logged in.");
	}

	public void paint(Graphics g) {
		gContext.setColor(Color.white);
     	gContext.fillRect(0, 0, width, height);

		if(loggedin) {
			int counter = 0;
			// System.out.println("This is the update and we are updating the pic");
			// queue.Display();

			for(Node temp = queue.head; temp != null; temp = temp.next) {
				if(temp.maxin > temp.maxout) {
					gContext.setColor(Color.red);
					gContext.drawLine(counter%width, bar, counter%width, get_pix(temp.maxin)); 
					gContext.setColor(Color.blue);
					gContext.drawLine(counter%width, bar, counter%width, get_pix(temp.maxout)); 
				} else {
					gContext.setColor(Color.blue);
					gContext.drawLine(counter%width, bar, counter%width, get_pix(temp.maxout)); 
					gContext.setColor(Color.red);
					gContext.drawLine(counter%width, bar, counter%width, get_pix(temp.maxin)); 
				}
				counter++;
			}

			gContext.setColor(Color.red);
			gContext.drawLine(0, get_pix(Integer.parseInt(maxin)), width, get_pix(Integer.parseInt(maxin))); 

			gContext.setColor(Color.blue);
			gContext.drawLine(0, get_pix(Integer.parseInt(maxout)), width, get_pix(Integer.parseInt(maxout))); 


			System.out.println("This is the current in " + curin);
			System.out.println("This is the current in " + curout);
			System.out.println("This is the current in " + maxin);
			System.out.println("This is the current in " + maxout);
			float percentin = ((float)Integer.parseInt(curin) / (float)Integer.parseInt(maxin));
			float percentout = ((float)Integer.parseInt(curout) / (float)Integer.parseInt(maxout));


			if(percentin < .5) gContext.setColor(new Color(percentin*2, (float)1.0, (float)0.0));
			else if(percentin < 1) gContext.setColor(new Color((float)1.0, ((1-percentin)*2), (float)0.0));
			else { gContext.setColor(Color.red); percentin = 1; }
			gContext.fillRect(0, bar+2, (int)(width*percentin), ((height - bar)/2)-2);

			if(percentout < .5) gContext.setColor(new Color(percentout*2, (float)1.0, (float)0.0));
			else if(percentout < 1) gContext.setColor(new Color((float)1.0, ((1-percentout)*2), (float)0.0));
			else { gContext.setColor(Color.red); percentout = 1; }
			gContext.fillRect(0, ((height - bar)/2)+bar+1, (int)(width*percentout), height-2);

			gContext.setColor(Color.black);
			gContext.drawString("Current In Used: " + (int)(percentin*100) + "% (" + Integer.parseInt(curin) / meg + "MB)", 5, bar+12);
			gContext.drawString("Current Out Used: " + (int)(percentout*100) + "% (" + Integer.parseInt(curout) / meg + "MB)", 5, bar+26);

			gContext.drawRect(0, bar+1, width-1, barwidth-2);
			gContext.drawLine(0, ((height - bar)/2)+bar, width, ((height - bar)/2)+bar);

			gContext.drawString("Max In is 300MB", 5, 10);
			gContext.drawString("Max Out is 150MB", 5, (bar/2)+10);


			// gContext.drawString("This is bandwidth info for " + ip, 60, 30);
			g.drawImage(buffer, 0, 0, this);
		}
	}

	public int get_pix(int in) {
		return (int)(bar - (int)(((long)((long)in * (long)(bar)) / (long)Integer.parseInt(maxin))));
	}

   public void update(Graphics g) { paint(g); }

	public boolean sendtoken() {
		try {
			fred = new Socket("is-dorm.southern.edu", 150);
			BufferedWriter outbound = new BufferedWriter(new OutputStreamWriter(fred.getOutputStream()));
			BufferedReader inbound = new BufferedReader(new InputStreamReader(fred.getInputStream()));
			outbound.write("token-" + token);
			outbound.newLine();
			outbound.flush();
			ip = inbound.readLine();	
			if(ip == null) return false;
			if(ip.compareTo("Unautherized") == 0) {
				fred.close();
				return false;
			}
			else if(ip.compareTo("Free") == 0) {
				if(debug) System.out.println("This is the free time get used to it");
				fred.close();
				return false;
			} else {
				token = ip;
				curin = inbound.readLine();	
				curout = inbound.readLine();	
				if(curin != null && curout != null) queue.push(Integer.parseInt(curin), Integer.parseInt(curout));  
				else System.out.println("There was a problem reading the in and out val"); 
				if(debug) System.out.println("There are the vals " + curin + " " + curout + " ");
				fred.close();
				return true;
			}
		} catch (UnknownHostException host) {
			System.out.println("Unknown host");
		} catch (IOException error) {
			System.out.println("Server Not Responding");
		} 
		return false;
	}

	public boolean login(String username, String password) {
		System.out.println("Username is: " + username + " password: " + password);
		try {
			fred = new Socket("is-dorm.southern.edu", 150);
			BufferedWriter outbound = new BufferedWriter(new OutputStreamWriter(fred.getOutputStream()));
			BufferedReader inbound = new BufferedReader(new InputStreamReader(fred.getInputStream()));
			outbound.write(username);
			outbound.newLine();
			outbound.flush();
			outbound.write(password);
			outbound.newLine();
			outbound.flush();
			ip = inbound.readLine();	

			System.out.println("This is out IP " + ip);

			if(ip == null) return false;

			if(ip.compareTo("Unautherized") == 0) {
				fred.close();
				return false;
			}
			else if(ip.compareTo("Free") == 0) {
				fred.close();
				return false;
			} else {
				token = ip;
				maxin = inbound.readLine();	
				maxout = inbound.readLine();	
				curin = inbound.readLine();	
				curout = inbound.readLine();	
				if(debug) System.out.println("There are the max vals " + maxin + " " + maxout + " " + curin + " " + curout + " ");
				fred.close();
				if(running == null) {
					if(debug) System.out.println("We are starting a thread for this thing");
					running = new Thread(this);
					running.start();
				}
				return true;
			}
		} catch (UnknownHostException host) {
			System.out.println("Unknown host");
		} catch (IOException error) {
			System.out.println("Server Not Responding");
		} 
		return false;
	}

//	protected static void delay(int millis) {
//		long startTime = System.currentTimeMillis(), stopTime;
//		while ( System.currentTimeMillis() - startTime < millis ) {}
//	}

	public static void main(String[] args) {
		Applet applet = new Bandwidth();
		Frame frame = new Frame("Fias Applet");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.add(applet);
		frame.setSize(height+1, width+1);
		frame.setBackground(Color.white);
		applet.init();
		applet.start();
		frame.setVisible(true);

	}
}

