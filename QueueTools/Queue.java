package QueueTools;

public class Queue {

	public Node head, tail;
	int count = 0;
	int maxsize;

	public Queue(int fullsize) {
		head = tail = null;
		maxsize = fullsize;
	}

	public boolean isFull() {
		if(count == maxsize) { return true; }
		return false;
	}

	public boolean push(int in, int out) {
		if(isFull()) { pop(); }
		Node new_node = new Node(in, out);
		if ( new_node == null ) {
			System.out.println("Out of Memory to create any more Nodes.");
			return false;
		}
		if(head == null || tail == null) {
			head = tail = new_node;
			new_node.next = new_node.prev = null;
			count++;
			return true;
		} else {
			new_node.next = null;
			new_node.prev = tail;
			tail.next = new_node;
			tail = new_node;
			count++;
			return true;
		}
	}

	public Node pop() {
		Node node; 
		if(head == null || tail == null) {
			return null;
		}
		if(head == tail) {
			node = head;
			head = tail = null;
			count--;
			return node;
		} else {
			node = head;
			head = head.next;
			head.prev = null;
			node.next = null;
			count--;
			return node;
		}
	}

	public void Display() {
		for(Node temp = head; temp != null; temp = temp.next) {
			System.out.println("(" + temp.maxin + "," + temp.maxout + ")");

		}
	}
}








