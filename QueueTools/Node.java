package QueueTools;

	public class Node {
		public int maxin, maxout;
		public Node next, prev;
		public Node(int in, int out) {
			maxin = in;
			maxout = out;
			prev = next = null;
		}
	}
