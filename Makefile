all:  clean build

build: Bandwidth.java
	javac Bandwidth.java

run: clean build
	java Bandwidth

clean:
	rm -fr *.class
	rm -fr QueueTools/*.class
